{ config, lib, pkgs, ... }: with lib; let
  cfg = config.services.yarr;
in {
  options = {
    services.yarr = {
      enable = mkEnableOption "yarr";

      package = mkOption {
        type = types.package;
        default = pkgs.yarr;
        defaultText = literalExpression "pkgs.yarr";
        description = "Yarr package to use.";
      };

      environmentFile = mkOption {
        type = types.path;
        description = ''
          File containing config environment variables starting with YARR_
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [
      cfg.package
    ];

    users.users.yarr = {
      description = "Yarr user";
      group = "yarr";
      isSystemUser = true;
    };
    users.groups.yarr = {};

    systemd.services.yarr = {
      description = "Yarr Feed Reader service";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];

      serviceConfig = {
        ExecStart = "${cfg.package}/bin/yarr";
        User = "yarr";
        DynamicUser = true;
        StateDirectory = "yarr";
        StateDirectoryMode = "0700";
        Environment = [
          "XDG_CONFIG_HOME=%S"
        ];
        EnvironmentFile = cfg.environmentFile;
        # Hardening
        CapabilityBoundingSet = [ "" ];
        DeviceAllow = [ "" ];
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        PrivateDevices = true;
        PrivateUsers = true;
        ProcSubset = "pid";
        ProtectClock = true;
        ProtectControlGroups = true;
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        ProtectProc = "invisible";
        RestrictAddressFamilies = [ "AF_INET" "AF_INET6" "AF_UNIX" ];
        RestrictNamespaces = true;
        RestrictRealtime = true;
        RestrictSUIDSGID = true;
        SystemCallArchitectures = "native";
        SystemCallFilter = [ "@system-service" "~@privileged" ];
        UMask = "0077";
      };
    };
  };
}