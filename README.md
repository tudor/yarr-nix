# Yarr, packaged for Nix and NixOS

This is a Nix flake for [yarr, yet another rss reader][yarr].
It comes with an overlay, exposed packages for `x86_64-linux` and `aarch64-linux`
(but you can use the overlay with any system), and a NixOS module to easily
install it on your server.

[yarr]: https://github.com/nkanaev/yarr
